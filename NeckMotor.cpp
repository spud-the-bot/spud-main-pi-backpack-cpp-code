//-----------------------------------------------------------------------------------------
// NeckMotor.cpp
//
// Controls the neck turning motor.
//
//
//----------------------------------------------------
//
// Authors:
//    Mike Schoonover 10/24/2020
//
//-----------------------------------------------------------------------------------------


#include "NeckMotor.h"

//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
// class NeckMotor
//
// This class controls the neck turning motor.
//

//-----------------------------------------------------------------------------------------
// NeckMotor::NeckMotor (constructor)
//

NeckMotor::NeckMotor() : 
	motorDriveCurrentLevel(MotorCurrentLevel::MA_500),
	motorDriveCurrentPWMLevel(0),
	motorStepDelay(1),
	randomNeckTurnsEnabled(false),
	fullNeckTurnsEnabled(false),
	neckTurnSpecifiedStepsEnabled(false)
	
{

}// end of NeckMotor::NeckMotor (constructor)
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::init
//
// top-level init that handles basic initialization and calls initSelf for specific init
// Must be called after instantiation to complete setup.
//

void NeckMotor::init()
{

	setupIO();

	initMotorParameters();

	// if analog input pin 0 is unconnected, random analog
	// noise will cause the call to randomSeed() to generate
	// different seed numbers each time the sketch runs.
	// randomSeed() will then shuffle the random function.
	randomSeed(analogRead(0));

}// end of NeckMotor::init
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::initMotorParameters
//
/**
 * Initializes various motor control parameters to known state.
 *
 */

void NeckMotor::initMotorParameters()
{

	stepCount = 0;

	pauseCount = 0;

	currentDir = FWD;

	currentRotationPos = 0;

	digitalWrite(NECK_MOTOR_STEP_PIN, LOW);

	digitalWrite(NECK_MOTOR_DIR_PIN, currentDir);

	digitalWrite(NECK_MOTOR_SLEEP_PIN, SLEEP_MODE);

}// end of NeckMotor::initMotorParameters
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::process
//
/**
 * This method should be called often to allow it to handle neck motor actions.
 *
 */

void NeckMotor::process()
{

	if(getRandomNeckTurnsEnabled()){ handleRandomNeckTurnRoutine(); }

	if(getFullNeckTurnsEnabled()){ handleFullNeckTurnRoutine(); }

	if(getNeckTurnSpecifiedStepsEnabled()) { handleNeckTurnSpecifiedSteps(); }

}// end of NeckMotor::process
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::enableRandomHeadTurningRoutine
//
/**
 * Initializes and starts the Random Head Turning routine. Function handleRandomNeckTurnRoutine()
 * should then be called repeatedly to allow the routine to operate.
 *
 * @param pEnable	true to start the routine, false to stop it
 *
 */

void NeckMotor::enableRandomHeadTurningRoutine(const bool pEnable)
{

	if (pEnable){
		initMotorParameters();
		setRandomNeckTurnsEnabled(true);

	}else {
		setRandomNeckTurnsEnabled(false);
		setMotorSleepMode(MOTOR_SLEEP);
	}

}// end of NeckMotor::enableRandomHeadTurningRoutine
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::enableFullHeadTurningRoutine
//
/**
 * Initializes and starts the Full Head Turning routine. Function handleFullNeckTurnRoutine()
 * should then be called repeatedly to allow the routine to operate.
 *
 * The first turn will be in the FWD direction to the exreme position. Subsequent turns will be
 * from one extreme to the other, back and forth. Thus the head should be moved to home position
 * before this routine is started.
 *
 * wip mks ~ in the future, head should automatically be moved to home before the routine starts
 *
 * @param pEnable	true to start the routine, false to stop it
 *
 */

void NeckMotor::enableFullHeadTurningRoutine(const bool pEnable)
{

	if (pEnable){
		initMotorParameters();
		currentDir = FWD;
		stepCount = ROTATE_MAX_POS;
		digitalWrite(NECK_MOTOR_DIR_PIN, currentDir);
		digitalWrite(NECK_MOTOR_SLEEP_PIN, WAKE_MODE);
		setFullNeckTurnsEnabled(true);
	}else {
		setFullNeckTurnsEnabled(false);
		setMotorSleepMode(MOTOR_SLEEP);
	}

}// end of NeckMotor::enableFullHeadTurningRoutine
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::startNeckTurnSpecifiedSteps
//
/**
 * Initializes and starts turning the neck for pNumSteps number of motor steps at a speed
 * controlled by delaying pNeckStepDelayMS between each step pulse transition.
 *
 * Function handleNeckTurnSpecifiedSteps() should then be called repeatedly to allow the routine to
 * operate.
 *
 * If pNumSteps is zero, the routine will still be enabled but the head will not actually turn.
 * Doing this allows the routine-completed packet to be returned as expected by the host without
 * extra code.
 * 
 * The neck will stop turning if it reaches a max limit.
 *
 * A routine-completed packet will be sent to the host when the turning completes, regardless of
 * the reason.
 *
 * @param pNumSteps			number of steps to turn the neck - positive turns left, negative turns
 *							right
 *
 * @param pNeckStepDelayMS	number of milliseconds to delay between each transition of the stepper
 *							motor driver step pulse; controls speed
 *
 */

void NeckMotor::startNeckTurnSpecifiedSteps(const int pNumSteps, const int pNeckStepDelayMS)
{

	initMotorParameters();
	stepCount = abs(pNumSteps);

	if(pNumSteps < 0){ currentDir = FWD; } else { currentDir = REV; }

	motorStepDelay = pNeckStepDelayMS;

	digitalWrite(NECK_MOTOR_DIR_PIN, currentDir);
	digitalWrite(NECK_MOTOR_SLEEP_PIN, WAKE_MODE);

	setNeckTurnSpecifiedStepsEnabled(true);

}// end of NeckMotor::startNeckTurnSpecifiedSteps
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::setMotorSleepMode
//
/**
 * Puts the motor in or out of Sleep mode.
 *
 * @param @Sleep	MOTOR_SLEEP to put motor driver in sleep mode, MOTOR_WAKE to wake motor
 *
 */

void NeckMotor::setMotorSleepMode(const bool pSleep)
{

	digitalWrite(NECK_MOTOR_SLEEP_PIN, (pSleep == MOTOR_SLEEP ? SLEEP_MODE : WAKE_MODE));

}// end of NeckMotor::setMotorSleepMode
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::handleRandomNeckTurnRoutine
//
/**
 * Handles the Random Neck Turn Routine which turns the head in random directions at random degrees
 * at random intervals. Should be called often when the process is active.
 *
 */

void NeckMotor::handleRandomNeckTurnRoutine()
{

	digitalWrite(LED_BUILTIN, LED_ON);

	rotateNeck();

	delay(10);

	digitalWrite(LED_BUILTIN, LED_OFF);

	digitalWrite(NECK_MOTOR_STEP_PIN, LOW);

	pauseRotating();

	generateNewRandomMoveOrPause();

	delay(10);

}// end of NeckMotor::handleRandomNeckTurnRoutine
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::handleFullNeckTurnRoutine
//
/**
 * Handles the Full Neck Turn Routine which turns the head fully in one direction, then fully
 * to the other direction, then back to center. Should be called often when the process is active.
 *
 */

void NeckMotor::handleFullNeckTurnRoutine()
{

	digitalWrite(LED_BUILTIN, LED_ON);

	rotateNeck();

	delay(1);

	digitalWrite(LED_BUILTIN, LED_OFF);

	digitalWrite(NECK_MOTOR_STEP_PIN, LOW);

	generateNewFullMove();

	delay(1);

}// end of NeckMotor::handleFullNeckTurnRoutine
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::handleNeckTurnSpecifiedSteps
//
/**
 * Handles the Full Neck Turn Routine which turns the head fully in one direction, then fully
 * to the other direction, then back to center. Should be called often when the process is active.
 *
 */

void NeckMotor::handleNeckTurnSpecifiedSteps()
{

	digitalWrite(LED_BUILTIN, LED_ON);

	rotateNeck();

	delay(motorStepDelay);

	digitalWrite(LED_BUILTIN, LED_OFF);

	digitalWrite(NECK_MOTOR_STEP_PIN, LOW);

	delay(motorStepDelay);

	if(stepCount == 0) {
		setNeckTurnSpecifiedStepsEnabled(false);
		setMotorSleepMode(MOTOR_SLEEP);
	}

}// end of NeckMotor::handleNeckTurnSpecifiedSteps
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::setMotorDriveCurrentLevel
//
/**
 * Sets the I1/I2 pins of the motor driver to select the desired motor drive current level. One of
 * the preset values can be specified or any number from 0~255 can be supplied instead to PWM
 * I1 to specify a specific level.
 *
 * To specify one of the preset levels, pLevel is passed as MotorCurrentLevel::MA_500,
 * MotorCurrentLevel::MA_1000, MotorCurrentLevel::MA_1500, OR MotorCurrentLevel::MA_2000.
 *
 * To specify a PWM value, pLevel is passed as MotorCurrentLevel::PWM and PWMDutyCycle is passed
 * as 0~255, where 0 is minimum current (0 Amps) and 255 is maximum current (~2 Amps).
 *
 *
 *		I1		I2		Current Limit
 *		-----------------------------
 *		Z		Z		0.5 A
 *		Low		Z		1 A
 *		Z		Low		1.5 A
 *		Low		Low		2 A
 *		PWM		Low		variable
 *
 * @param pLevel		a MotorCurrentLevel enum value which specifies one of a set of possible
 *						drive current levels or PWM if the level is to be set by PWM output
 * @param pPWMDutyCycle	the value to be used as the PWM output (0~255) to the motor driver to
 *						set the current drive level; only applies if pLevel = PWM
 *
 * @return				0 if no errors
 *
 */

int NeckMotor::setMotorDriveCurrentLevel(const MotorCurrentLevel pLevel, const int pPWMDutyCycle)
{

	motorDriveCurrentLevel = pLevel;

	int dutyCycle = pPWMDutyCycle;

	setMotorDriveCurrentPWMLevel(dutyCycle);

	switch(pLevel){

	case MotorCurrentLevel::MA_500 :

		pinMode(NECK_MOTOR_I1_PIN, INPUT); //set to floating by setting to input
		pinMode(NECK_MOTOR_I2_PIN, INPUT); //set to floating by setting to input

	flashLED(1, 300); //debug mks

		return(0);

	break;

	case MotorCurrentLevel::MA_1000 :

		pinMode(NECK_MOTOR_I1_PIN, OUTPUT);
		digitalWrite(NECK_MOTOR_I1_PIN, LOW);
		pinMode(NECK_MOTOR_I2_PIN, INPUT); //set to floating by setting to input

	flashLED(2, 300); //debug mks

		return(0);

	break;

	case MotorCurrentLevel::MA_1500 :

		pinMode(NECK_MOTOR_I1_PIN, INPUT); //set to floating by setting to input
		pinMode(NECK_MOTOR_I2_PIN, OUTPUT);
		digitalWrite(NECK_MOTOR_I2_PIN, LOW);

	flashLED(3, 300); //debug mks

		return(0);

	break;

	case MotorCurrentLevel::MA_2000 :

		pinMode(NECK_MOTOR_I1_PIN, OUTPUT);
		digitalWrite(NECK_MOTOR_I1_PIN, LOW);
		pinMode(NECK_MOTOR_I2_PIN, OUTPUT);
		digitalWrite(NECK_MOTOR_I2_PIN, LOW);

	flashLED(4, 300); //debug mks

		return(0);

	break;

	case MotorCurrentLevel::PWM :

		pinMode(NECK_MOTOR_I2_PIN, OUTPUT);
		digitalWrite(NECK_MOTOR_I2_PIN, LOW);

		if(dutyCycle < 0 || dutyCycle > 255) { dutyCycle = 0; }

		analogWrite(NECK_MOTOR_I1_PIN, dutyCycle);

	flashLED(5, 300); //debug mks

		return(0);

	break;

	default:

		//unknown mode gives lowest current level

		pinMode(NECK_MOTOR_I1_PIN, INPUT); //set to floating by setting to input
		pinMode(NECK_MOTOR_I2_PIN, INPUT); //set to floating by setting to input

		return(0);

	break;

	}

}// end of NeckMotor::setMotorDriveCurrentLevel
//-----------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::setupIO
//
/**
 *  Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
 *  as SPI, U2C, or PWM are set up in other functions.
 *
 */

void NeckMotor::setupIO()
{

	pinMode(LED_BUILTIN, OUTPUT); // initialize digital pin LED_BUILTIN as an output.

	pinMode(NECK_MOTOR_DIR_PIN, OUTPUT);
	pinMode(NECK_MOTOR_STEP_PIN, OUTPUT);
	pinMode(NECK_MOTOR_SLEEP_PIN, OUTPUT);
	pinMode(NECK_MOTOR_FAULT_PIN, INPUT);

	pinMode(NECK_MOTOR_I1_PIN, INPUT); //leave floating by setting to input
	pinMode(NECK_MOTOR_I2_PIN, INPUT); //leave floating by setting to input

}// end NeckMotor::setupIO
//-----------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
// NeckMotor::pauseRotating
//

void NeckMotor::pauseRotating()
{

	if(pauseCount == 0 && stepCount == 0) {

	  pauseCount = random(200);

	  digitalWrite(NECK_MOTOR_SLEEP_PIN, SLEEP_MODE);

	  return;

	}

	if(pauseCount <= 0) { return; }

	pauseCount--;

	//Serial.print("pause rotating count: "); Serial.println(pauseCount);

}// end of NeckMotor::pauseRotating
//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
// NeckMotor::generateNewRandomMoveOrPause
//
/** 
* If previous move or pause is complete, sets up parameters to pause for random time or to turn
* neck a random distance in a random direction.
*
*/

void NeckMotor::generateNewRandomMoveOrPause()
{

	if(stepCount > 0 || pauseCount > 0) { return; }

	stepCount = random(ROTATE_MAX_POS);

	if(stepCount < (ROTATE_MAX_POS / 5)) { stepCount = (ROTATE_MAX_POS / 5); }

	long newRandomDir = random(2);

	if(newRandomDir == 0){ currentDir = FWD; } else  { currentDir = REV; }

	if((currentRotationPos + stepCount) > ROTATE_MAX_POS
											|| (currentRotationPos - stepCount) < -ROTATE_MAX_POS){

	  //Serial.println("out of bounds...going back to ZERO!!!");

	  //go back to zero position

	  stepCount = abs(currentRotationPos);

	  if(currentRotationPos > 0){ currentDir = REV; } else  { currentDir = FWD; }

	}

	digitalWrite(NECK_MOTOR_DIR_PIN, currentDir);

	digitalWrite(NECK_MOTOR_SLEEP_PIN, WAKE_MODE);

	//Serial.println("new move...");

}// end of NeckMotor::generateNewRandomMoveOrPause
//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
// NeckMotor::generateNewFullMove
//
// If previous move is complete, sets up parameters to turn neck fully in the opposite direction
// from the previous move.
//

void NeckMotor::generateNewFullMove()
{

	if(stepCount > 0) { return; }

	if(currentDir == FWD){
		currentDir = REV;
		stepCount = ROTATE_MAX_POS * 2;
	}else{
		currentDir = FWD;
		stepCount = ROTATE_MAX_POS * 2;
	}

	digitalWrite(NECK_MOTOR_DIR_PIN, currentDir);

	digitalWrite(NECK_MOTOR_SLEEP_PIN, WAKE_MODE);

	//Serial.println("new move...");

}// end of NeckMotor::generateNewFullMove
//-------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------
// NeckMotor::rotateNeck
//
/**
 * Rotates the neck one step by taking the motor driver Step pin high. The direction pin on the
 * motor driver should already have been set as appropriate. The position will be tracked by
 * incrementing or decrementing currentRotationPos per the direction of rotation.
 *
 * NOTE: client could/should set the Step pin back low??? Can that be done here? The pulse will
 * be long enough, need a pause between pulses to allow head to actually turn.
 *
 */

void NeckMotor::rotateNeck()
{

	if(stepCount <= 0) { return; }

	stepCount--;

	digitalWrite(NECK_MOTOR_STEP_PIN, HIGH);

	if(currentDir == FWD){ currentRotationPos++; } else { currentRotationPos--; }

	//Serial.print("rotate ");
	//Serial.print(currentDir == FWD ? "FWD " : "REV " );
	//Serial.print(stepCount); Serial.print( " position = "); Serial.println(currentRotationPos);

}// end of NeckMotor::rotateNeck
//-------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------------------
// NeckMotor::NeckMotor (destructor)
//

NeckMotor::~NeckMotor()
{

}// end of NeckMotor::NeckMotor (destructor)
//-----------------------------------------------------------------------------------------

//end of class NeckMotor
//-----------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------
