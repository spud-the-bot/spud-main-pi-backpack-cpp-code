
#ifndef _MAIN_CONTROLLER_H
#define _MAIN_CONTROLLER_H

#include <Arduino.h>


#include "ErrorFlags.h"
#include "Tools.h"
#include "PacketTool.h"
#include "HostSerialComHandler.h"
#include "NeckMotor.h"
#include "Settings.h"

//--------------------------------------------------------------------------------------------------
// class MainController
//
// holds information that is shared between Objects
//

class MainController{

	public:

		MainController();

		void init();

		void process();

		void setupIO();

		void initSerialPort();

		void doCommands();

		PacketStatusEnum handleSetNeckMotorDriveCurrentLevel();

		PacketStatusEnum handleNeckTurnSpecifiedSteps();

		void displayLoopExecutionTime(const int pNumLoopsRun);

		void debug();

		virtual ~MainController();


	protected:

		void handlePacket();

		void handleErrorFlags(const ErrorFlag pErrorFlag);

	public:

		static const uint8_t HOST_ADDRESS = 0;

		static const uint8_t THIS_DEVICE_ADDRESS_ON_NETWORK = 1;

		static const int SERIAL_TIMEOUT_MILLIS = 1000;

	protected:

		Settings *settings;

		HostSerialComHandler *hostCom;

		NeckMotor *neckMotor;

		uint32_t loopCount = 0, startTime = millis();

};// end of class MainController
//--------------------------------------------------------------------------------------------------

#endif // _MAIN_CONTROLLER_H