//--------------------------------------------------------------------------------------------------
// HostSerialComHandler.cpp
//
// This class handles serial communication between the Control board and the Host Computer.
//
// NOTE regarding SERIAL, SERIAL1, SERIAL2, SERIAL3
//
//		These CANNOT be instantiated as objects to be passed into this class. Thus, this class
//		is hardcoded to use Serial.
//
//
// NOTE
//
//	Function checkForPacketReady should be called often to prevent serial buffer overflow.
//
//----------------------------------------------------
//
// Authors:
//
//	Mike Schoonover		10/26/2020
//	Hunter Schoonover
//
//
// The Arduino serial buffers default to 64 bytes in size. This can be changed. Most of our code
// uses the Hardware Serial Ports. Note that the ATmega328 only has 2048 bytes of RAM.
//
//	Software Serial Buffer Expansion
//
//	The change for software serial ports require a simple modification of the file:
//
//		<base Arduino folder>\hardware\arduino\avr\libraries\SoftwareSerial\SoftwareSerial.h
//
//	Change:
//
//		__#define _SS_MAX_RX_BUFF 64 // RX buffer size
//			(what about TX buffer???)
//	To:
//
//		__#define _SS_MAX_RX_BUFF 256 // RX buffer size
//
//	Hardware Serial Buffer Expansion
//
//	The change for hardware serial ports require a simple modification of the file:
//
//		<base Arduino folder>\hardware\arduino\avr\cores\arduino\HardwareSerial.h
//
//	Change:
//
//		__#define SERIAL_TX_BUFFER_SIZE 64
//		__#define SERIAL_RX_BUFFER_SIZE 64
//
//	To:
//
//		__#define SERIAL_TX_BUFFER_SIZE 256
//		__#define SERIAL_RX_BUFFER_SIZE 256
//
//
//--------------------------------------------------------------------------------------------------


#include "HostSerialComHandler.h"

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class HostSerialComHandler
//
/**
 * This class handles serial communication between the Control board and the Host Computer.
 *
 */

//--------------------------------------------------------------------------------------------------
// HostSerialComHandler::HostSerialComHandler (constructor)
//
/**
 *
 * @param pAddress	address for this device on the network
 *
 */

HostSerialComHandler::HostSerialComHandler(const uint8_t pAddress, PacketTool * const pPacketTool)
{

	thisDeviceAddr = pAddress;

	packetTool = pPacketTool;

}// end of HostSerialComHandler::HostSerialComHandler (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostSerialComHandler::init
//
/**
 * Initializes the object. Should be called after instantiation.
 *
 */

void HostSerialComHandler::init()
{

	setupIO();

}// end of HostSerialComHandler::init
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostSerialComHandler::sendACKPkt
//
/**
 * Sends an ACK packet to the host with status code pStatus.
 *
 * @param pSourcePktType	the type of packet being acknowledged.
 * @param pStatus			the PacketStatusEnum value to be sent to host as data byte in packet
 *
 */

void HostSerialComHandler::sendACKPkt(PacketTypeEnum pSourcePktType, PacketStatusEnum pStatus)
{

	packetTool->sendBytes(PacketTypeEnum::ACK_PKT, 2, pSourcePktType, pStatus);

}// end of HostSerialComHandler::sendACKPkt
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostSerialComHandler::setupIO
//
/**
 * Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
 * as SPI, U2C, or PWM are set up in other functions.
 *
 */

void HostSerialComHandler::setupIO()
{

}// end HostSerialComHandler::setupIO
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// HostSerialComHandler::HostSerialComHandler (destructor)
//

HostSerialComHandler::~HostSerialComHandler()
{

}// end of HostSerialComHandler::HostSerialComHandler (destructor)
//--------------------------------------------------------------------------------------------------

//end of class HostSerialComHandler
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
