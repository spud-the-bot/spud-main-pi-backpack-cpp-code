
#ifndef _NECK_MOTOR_H
#define _NECK_MOTOR_H

#include <Arduino.h>


#include "ErrorFlags.h"
#include "Tools.h"

enum class MotorCurrentLevel{
	PWM = 0,
	MA_500 = 1,
	MA_1000 = 2,
	MA_1500 = 3,
	MA_2000 = 4
};

//-----------------------------------------------------------------------------------------
// class NeckMotor
//
// This class controls the neck turning motor.
//

class NeckMotor{

	public:

		NeckMotor();

		void init();

		void process();

		void setupIO();

		void enableRandomHeadTurningRoutine(const bool pEnable);

		void enableFullHeadTurningRoutine(const bool pEnable);

		void startNeckTurnSpecifiedSteps(const int pNumSteps, const int pNeckStepDelayMS);

		void setMotorSleepMode(const bool pSleep);

		void setRandomNeckTurnsEnabled(const bool pValue){ randomNeckTurnsEnabled = pValue; }

		void setFullNeckTurnsEnabled(const bool pValue){ fullNeckTurnsEnabled = pValue; }

		void setNeckTurnSpecifiedStepsEnabled(const bool pValue){
			neckTurnSpecifiedStepsEnabled = pValue;
		}

		boolean getRandomNeckTurnsEnabled(){ return(randomNeckTurnsEnabled); }

		boolean getFullNeckTurnsEnabled(){ return(fullNeckTurnsEnabled); }

		boolean getNeckTurnSpecifiedStepsEnabled(){ return(neckTurnSpecifiedStepsEnabled); }

		MotorCurrentLevel getMotorDriveCurrentLevel(){ return(motorDriveCurrentLevel); }

		int setMotorDriveCurrentLevel(const MotorCurrentLevel pLevel, const int pPWMDutyCycle);

		int16_t getMotorDriveCurrentPWMLevel(){ return(motorDriveCurrentPWMLevel); }

		void setMotorDriveCurrentPWMLevel(int16_t pValue){ motorDriveCurrentPWMLevel = pValue; }

		int getStepCount(){ return(stepCount); }

		virtual ~NeckMotor();


	protected:

		void handleRandomNeckTurnRoutine();
		void handleFullNeckTurnRoutine();
		void handleNeckTurnSpecifiedSteps();
		void initMotorParameters();
		void pauseRotating();
		void generateNewRandomMoveOrPause();
		void generateNewFullMove();
		void rotateNeck();

	public:
		
		const bool MOTOR_SLEEP = true;
		const bool MOTOR_WAKE = false;

	protected:

		const int NECK_MOTOR_DIR_PIN = 9;
		const int NECK_MOTOR_STEP_PIN = 8;
		const int NECK_MOTOR_SLEEP_PIN = 7;
		const int NECK_MOTOR_FAULT_PIN = 10;

		const int NECK_MOTOR_I1_PIN = 3;
		const int NECK_MOTOR_I2_PIN = 6;

		const int SLEEP_MODE = LOW;
		const int WAKE_MODE = HIGH;

		const int LED_OFF = LOW;
		const int LED_ON = HIGH;

		const int FWD = HIGH;
		const int REV = LOW;

		const int ROTATE_MAX_POS = 770;	//70 without 11:1 gearbox, 70*11 with gearbox

		MotorCurrentLevel motorDriveCurrentLevel;

		int16_t motorDriveCurrentPWMLevel;

		int stepCount;

		int currentDir;

		long randomVal;

		long pauseCount;

		int currentRotationPos;

		int motorStepDelay;

		bool randomNeckTurnsEnabled;

		bool fullNeckTurnsEnabled;

		bool neckTurnSpecifiedStepsEnabled;


};// end of class NeckMotor
//-----------------------------------------------------------------------------------------

#endif // _NECK_MOTOR_H