
#ifndef _HOST_SERIAL_COM_HANDLER_H
#define _HOST_SERIAL_COM_HANDLER_H

#include <Arduino.h>
#include <stdint.h>

#include "PacketTool.h"
#include "PacketStatusEnum.h"
#include "Tools.h"

//--------------------------------------------------------------------------------------------------
// class HostSerialComHandler
//
// This class handles serial communication between the Control board and the Host Computer.
//

class HostSerialComHandler{

	public:


public:

		HostSerialComHandler(const uint8_t pAddress, PacketTool * const packetTool);

		void init();

		virtual ~HostSerialComHandler();

		void setupIO();

		PacketTool *getPacketTool(){ return(packetTool); }

		void sendACKPkt(PacketTypeEnum pSourcePktType, PacketStatusEnum pStatus);

	protected:

		
	protected:

		PacketTool *packetTool;

		uint8_t thisDeviceAddr;

	public:


};// end of class HostSerialComHandler
//--------------------------------------------------------------------------------------------------

#endif // _HOST_SERIAL_COM_HANDLER_H