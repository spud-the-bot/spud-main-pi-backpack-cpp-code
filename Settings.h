
#ifndef _SETTINGS_H
#define _SETTINGS_H

#include <Arduino.h>

//-----------------------------------------------------------------------------------------
// class Settings
//
// This class handles settings used throughout the program.
//

class Settings{

	public:

		Settings();

		void init();

		virtual ~Settings();

		int getNeckMotorCurrentLevel(){ return(neckMotorCurrentLevel); }

		void setNeckMotorCurrentLevel(int pValue){ neckMotorCurrentLevel = pValue; }

	protected:

		int neckMotorCurrentLevel;

	protected:


};// end of class Settings
//-----------------------------------------------------------------------------------------

#endif // _SETTINGS_H