//--------------------------------------------------------------------------------------------------
// PacketTool.cpp
//
// This class handles formatting, reading, and writing of packets.
//
//
//----------------------------------------------------
//
// Authors:
//    Mike Schoonover 10/24/2020
//
//--------------------------------------------------------------------------------------------------

#include "PacketTool.h"

#include <stdarg.h>

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class PacketTool
//
// This class handles formatting, reading, and writing of packets.
//

//--------------------------------------------------------------------------------------------------
// PacketTool::PacketTool (constructor)
//

PacketTool::PacketTool() : resyncCount(0), headerValid(false), numDataBytesRead(0)

{

	pktType = PacketTypeEnum::NO_PKT;

}// end of PacketTool::PacketTool (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::init
//
// Must be called after instantiation to complete setup.
//

void PacketTool::init(const uint8_t pHostAddress, const uint8_t pThisDeviceAddress)
{

	hostAddress = pHostAddress;

	thisDeviceAddr = pThisDeviceAddress;

}// end of PacketTool::init
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::setStreams
//
/**
 * Sets the input and output streams for the communication port.
 *
 * @param pInputStream			InputStream for the communications port
 * @param pOutputStream			OutputStream for the communications port
 *
 */

void PacketTool::setStreams(Stream * const pInputStream, Stream * const pOutputStream)
{

	byteIn =  pInputStream;
	byteOut = pOutputStream;

}// end of PacketTool::setStreams
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::checkForPacketReady
//
/**PacketType
 * Checks to see if a full packet has been received. Returns true if a complete packet has been
 * received, the checksum is valid, and the packet is addressed to this device.
 *
 * If ready, the packet type can be accessed by calling getPktType(). The number of data bytes in
 * the packet can be retrieved by calling getNumPktDataBytes(). The data bytes buffer can be
 * accessed by calling getPktDataBuffer().
 *
 * If the checksum for a packet is invalid, function will return false.
 *
 * If the function returns false for any reason the state of the pktType, packet data buffer,
 * and numPktDatabytes are undefined in that case.
 *
 * If enough bytes are waiting in the receive buffer to form a packet header, those bytes are
 * retrieved and the header is analyzed. If enough bytes are waiting in the receive buffer to
 * complete the entire packet based on the number of data bytes specified in the header, those
 * bytes are retrieved and this function returns true if the checksum is valid.
 *
 * If a packet header can be read but the full packet is not yet available, the header info is
 * stored for use in succeeding calls which will keep checking for enough bytes to complete the
 * packet. The function will return false until the full packet has been read.
 *
 * If 0xaa,0x55 is not found when the start of a header is expected, the buffer will be stripped of
 * bytes until it is empty or 0xaa is found. The stripped bytes will be lost forever.
 *
 * NOTE		This function should be called often to prevent serial buffer overflow!
 *
 * @return	true if a full packet with valid checksum is ready, false otherwise
 *
 */

bool PacketTool::checkForPacketReady()
{

	if (!headerValid){
		checkForPacketHeaderAvailable();
		if(!headerValid) { return(false); }
	}

	int bytesAvailable = byteIn->available();

	if (bytesAvailable <= 0){ return(false); }

	//don't read more than needed to complete the packet
	if (bytesAvailable > (numDataBytesPlusChecksumByte - numDataBytesRead)){
		bytesAvailable = numDataBytesPlusChecksumByte - numDataBytesRead;
	}

	byteIn->readBytes(inBuffer + numDataBytesRead, bytesAvailable);

	numDataBytesRead += bytesAvailable;

	if(numDataBytesRead < numDataBytesPlusChecksumByte){ return(false); }

	headerValid = false; //reset for next packet

	if(destDeviceAddrFromReceivedPkt != thisDeviceAddr){ return(false); }

	for(int i=0; i<numDataBytesPlusChecksumByte; i++){ pktChecksum += inBuffer[i]; }

	if ((pktChecksum & 0xff) != 0) { return(false); }

	return(true);

}// end of PacketTool::checkForPacketReady
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::checkForPacketHeaderAvailable
//
/**
 * Checks to see if a header is available in the stream buffer. At least 7 bytes must be available
 * and the first two bytes must be 0xaa,0x55.
 *
 * If a valid header is found, other functions can detect this state by checking if
 *  (headerValid == true).
 *
 * If a valid header is found, destDeviceAddrFromReceivedPkt, sourceDeviceAddrFromReceivedPkt,
 * pktType, and numPktDataBytes will be set to the values specified in the header. The bytes in the
 * header will be summed and stored in pktCheckSum.
 *
 * For convenience, numDataBytesPlusChecksumByte will be set to (numPktDataBytes + 1).
 *
 * If 0xaa,0x55 is not found when the start of a header is expected, the buffer will be stripped of
 * bytes until it is empty or 0xaa is found. The stripped bytes will be lost forever. The next
 * call to checkForPacketReady will then attempt to read the header or toss more bytes if
 * more invalid data has been received by then.
 *
 * There is no way to verify the header until the entire packet is read. The packet checksum
 * includes the header, so at that time the entire packet can be validated or tossed.
 *
 */

void PacketTool::checkForPacketHeaderAvailable()
{

	if(byteIn->available() < 7){ return; }

	pktChecksum = 0;

	if(byteIn->read() != 0xaa){ resync(); return; }

	pktChecksum += 0xaa;

	if(byteIn->read() != 0x55){ resync(); return; }

	pktChecksum += 0x55;

	destDeviceAddrFromReceivedPkt = byteIn->read();

	pktChecksum += destDeviceAddrFromReceivedPkt;

	sourceDeviceAddrFromReceivedPkt = byteIn->read();

	pktChecksum += sourceDeviceAddrFromReceivedPkt;

	pktType = (PacketTypeEnum)(byteIn->read());

	pktChecksum += (int)pktType;

	uint8_t numPktDataBytesMSB = byteIn->read();

	pktChecksum += numPktDataBytesMSB;

	uint8_t numPktDataBytesLSB = byteIn->read();

	pktChecksum += numPktDataBytesLSB;

	numPktDataBytes =
		(uint16_t)((numPktDataBytesMSB<<8) & 0xff00) + (uint16_t)(numPktDataBytesLSB & 0xff);

	numDataBytesPlusChecksumByte = numPktDataBytes + 1;

	numDataBytesRead = 0;

	headerValid = true;

}// end of PacketTool::checkForPacketHeaderAvailable
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::resync
//
/**
 * Reads and tosses bytes from byteIn until 0xaa is found or the buffer is empty. If found, the
 * 0xaa byte is left in the buffer to be read by the next attempt to read the header.
 *
 * Increments resyncCount.
 *
 */

void PacketTool::resync()
{

	resyncCount++;

	while(byteIn->available() > 0){
		if(byteIn->peek() == 0xaa){ return; }
		byteIn->read();
	}

}// end of PacketTool::resync
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::prepareHeader
//
/**
 * Sets up a valid packet header at the beginning of pOutBuffer. The header includes:
 *
 *	0xaa, 0x55, <dest device address>, <this device address>, <packet type>,
 *					 <number of data bytes in packet (MSB)> <number of data bytes in packet (LSB)>
 *
 * The number of data bytes excludes this header and the checksum. Example full packet:
 *
 *	0xaa, 0x55, 0, 1, 1, 4, 5, 1, 2, 3, 4, 0x??
 *
 *	where:
 *		oxaa, 0x55 are identifier bytes used in all packet headers
 *		0 is the destination address (0 for HOST)
 *		1 is this device's address (obtained from HostSerialComHandler::address)
 *		1 is the packet type (will vary based on the type of packet)
  *		4 is the number of data bytes ~ upper byte of int
 *		5 is the number of data bytes ~ lower byte of int
 *		1,2,3,4 are the data bytes
 *		0x?? is the checksum for all preceding header and data bytes
 *
 * Note that this function only sets up the header in the buffer, the data bytes and checksum must
 * be added by the calling function.
 *
 * @param pOutBuffer		the buffer in which to store the header
 * @param pDestAddress		the address of the destination device
 * @param pPacketType		the packet type
 * @param pNumDataBytes		the number of data bytes that will be added to the packet
 *
 * @return					the index of the next empty spot in pOutBuffer
 *
 */

int PacketTool::prepareHeader( uint8_t * const pOutBuffer, const uint8_t pDestAddress,
								    const PacketTypeEnum pPacketType, const uint8_t pNumDataBytes)
{

	int x = 0;

	pOutBuffer[x++] = 0xaa;

	pOutBuffer[x++] = 0x55;

	pOutBuffer[x++] = pDestAddress;

	pOutBuffer[x++] = thisDeviceAddr;

	pOutBuffer[x++] = (uint8_t)pPacketType;

	pOutBuffer[x++] = uint8_t ((pNumDataBytes >> 8) & 0xff);

	pOutBuffer[x++] = uint8_t (pNumDataBytes & 0xff);

	return(x);

}//end of PacketTool::prepareHeader
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendBytes
//
/**
 * Sends a variable number of bytes (one or more) to the remote device, prepending a valid header
 * and appending the appropriate checksum.
 *
 * NOTE: Use sendCString to send a null terminated character array as sendBytes is a variatic
 * function which will not accept single-byte-size variable types.
 *
 * Does nothing if byteOut not open.
 *
 * NOTE: C++ Variadic functions (those with variable number of parameters) force you to use
 * a variable type of at least size int for the "...". If you try to use char, unint8_t, etc. the
 * compiler will generate a warning and the code will fail.
 *
 * @param pPacketType		the packet type
 * @param pNumBytes			the number of bytes to be sent
 *							the maximum allowable value is 255; if greater then will be set to 255
 * @param ...		the list of bytes to be sent; these actually must be ints and cannot be > 255!!!
 *
 */

void PacketTool::sendBytes(const PacketTypeEnum pPacketType, int pNumBytes, ...)
{

	if(!byteOut){ return; }

	if(pNumBytes > 255){ pNumBytes = 255; }

	int x = prepareHeader(outBuffer, hostAddress, pPacketType, (uint8_t)pNumBytes);

	va_list valist;

	va_start(valist, pNumBytes);

	int i;

    for(i=0; i<pNumBytes; i++){ outBuffer[x++] = va_arg(valist, int); }

	va_end(valist);

	int checksum = 0;

    for(int j=0; j<x; j++){ checksum += outBuffer[j]; }

    //calculate checksum and put at end of buffer
    outBuffer[x++] = (uint8_t)(0x100 - (uint8_t)(checksum & 0xff));

	byteOut->write(outBuffer, x);
	// do NOT use comPort->flush(); as it waits until data sent

	//debug mks
	//comPort->print("checksum: "); comPort->println(outBuffer[pNumBytes]);
	//debug mks end

}//end of PacketTool::sendBytes
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendCString
//
/**
 * Sends a C-String (null terminated character array) to the remote device, prepending a valid
 * header and appending the appropriate checksum.
 *
 * NOTE: This is nearly identical to sendBytes. It is required because sendBytes is a variatic
 * function which cannot accept single-byte-size values. As RAM memory is limited in some devices,
 * it is better to duplicate the function rather than waste RAM as a buffer to convert the C-String
 * to an integer array.
 *
 * Does nothing if byteOut not open.
 *
 * @param pPacketType		the packet type
 * @param pString			the C-String char array to be sent
 *
 */

void PacketTool::sendCString(const PacketTypeEnum pPacketType, const char * const pString)
{

	if(!byteOut){ return; }

	int numBytes = strlen(pString);

	if(numBytes > 254){ numBytes = 254; } //leave space for the null terminator

	int x = prepareHeader(outBuffer, hostAddress, pPacketType, (uint8_t)(numBytes + 1));

	int i;

    for(i=0; i<numBytes; i++){ outBuffer[x++] = pString[i]; }

	outBuffer[x++] = 0x00; //add null terminator

	int checksum = 0;

    for(int j=0; j<x; j++){ checksum += outBuffer[j]; }

    //calculate checksum and put at end of buffer
    outBuffer[x++] = (uint8_t)(0x100 - (uint8_t)(checksum & 0xff));

	byteOut->write(outBuffer, x);
	// do NOT use comPort->flush(); as it waits until data sent

}//end of PacketTool::sendCString
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::parseIntegerFromDuplexPacket
//
 /**
 * Extracts a two-byte signed integer from the current packet data in inBuffer starting at index
 * pIndex in the array. The integer is reconstructed from the two data bytes at that index
 * position.
 *
 * The integer is parsed using Big Endian order (MSB first).
 *
 * The value's copy is also extracted from the buffer immediately after the value itself. The two
 * are compared to verify integrity.
 *
 * @param pIndex	the starting position in inBuffer of the value to be parsed; will be updated
 *					to point to the next position after the value and its copy
 *
 * @param pValue	the value extracted from the buffer will be returned via this pointer
 *
 * @return			PacketStatusEnum::PACKET_VALID if no error
 *					PacketStatusEnum::DUPLEX_MATCH_ERROR if the two copies in the packet of the
 *					value do not match
 *
 */

PacketStatusEnum PacketTool::parseDuplexIntegerFromPacket(int *pIndex, int16_t *pValue)
{

    *pValue =
		(int16_t)((inBuffer[(*pIndex)++]<<8) & 0xff00) + (int16_t)(inBuffer[(*pIndex)++] & 0xff);

	int16_t copy;
	copy =
		(int16_t)((inBuffer[(*pIndex)++]<<8) & 0xff00) + (int16_t)(inBuffer[(*pIndex)++] & 0xff);

	PacketStatusEnum status;

	if(*pValue == copy){ status = PacketStatusEnum::PACKET_VALID; }
	else{ status = PacketStatusEnum::DUPLEX_MATCH_ERROR; }

	return(status);

}//end of PacketTool::parseIntegerFromDuplexPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::waitForNumberOfBytes
//
/**
 * Waits until pNumBytes number of data bytes are available in the stream byteIn or until the
 * specified number of milliseconds pTimeOutMillis has passed.
 *
 * @param pNumBytes			the number of bytes to wait for
 * @param pTimeOutMillis	the maximum number of milliseconds to wait
 * @return					the number of bytes available or -1 if time out occurred
 *
 */

int PacketTool::waitForNumberOfBytes(const int pNumBytes, const int pTimeOutMillis)
{

	unsigned long startTime = millis();

	while((millis() - startTime) < pTimeOutMillis){
		if (byteIn->available() >= pNumBytes) {return(byteIn->available());}
	}

	return(-1);

}//end of PacketTool::waitForNumberOfBytes
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::readBytes
//
/**
 * Retrieves pNumBytes number of data bytes from the stream byteIn and stores them in inBuffer.
 * Returns the number of characters placed in the buffer. A 0 means no valid data was found.
 * Will timeout based on previous call to setTimeout(SERIAL_TIMEOUT_MILLIS).
 *
 * @param pNumBytes	number of bytes to read
 * @return			number of bytes retrieved from the socket; if the attempt times out returns 0
 *
 */

int PacketTool::readBytes(const int pNumBytes)
{

	byteIn->readBytes(inBuffer, pNumBytes);

}//end of PacketTool::readBytes
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::PacketTool (destructor)
//

PacketTool::~PacketTool()
{

}// end of PacketTool::PacketTool (destructor)
//--------------------------------------------------------------------------------------------------

//end of class PacketTool
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
