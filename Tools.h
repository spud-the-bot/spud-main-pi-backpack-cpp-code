
#ifndef _TOOLS_H
#define _TOOLS_H

#include <Arduino.h>
//#include <string.h>     // necessary for dtostrf()
//#include <stdlib.h>     // necessary for dtostrf()
//#include <SdFat.h>
//#include <format>

extern void printDoubleToSerialPort( const double pVal, unsigned int pPrecision);

static void printBarToSerial(const char c, const int length) {
	for(int i = 0; i < length; i++) Serial.print(c); Serial.println();
};

extern uint32_t getFreeRAMSize();

extern void printFreeRAMSize();

extern int16_t getSineWaveValue(const int pDegrees, const int pMagnitude, const int8_t pNoiseLevel);

//char *dtostrf(double val, int width, unsigned int prec, char *sout);
//char *dtostrf(double val, signed char width, unsigned char prec, char *sout);

template <typename tT>
static tT window(tT value, tT min, tT max) {
	if(value < min) return min;
	if(value > max) return max;
	return value;
}

static bool startsWith(const char *string, const char *prefix) {
	while(*prefix) if(tolower(*prefix++) != tolower(*string++)) return false;
    return true;
}

static bool startsWithCaseSensitive(const char *string, const char *prefix) {
	while(*prefix) if(*prefix++ != *string++) return false;
    return true;
}

//--------------------------------------------------------------------------------------------------
// ::flashLED
//
/**
 * Flashes the onboard LED the number of times specified by pNumFlashes with pulse width specified
 * by pPulseWidth milliseconds.
 *
 * @pNumFlashes		number of times to flash the LED
 * @pPulseWidth		milliseconds used for both On and Off periods
 *
 */

static void flashLED(const int pNumFlashes, const int pPulseWidth)
{

	for(int i=0; i<pNumFlashes; i++){
		digitalWrite(LED_BUILTIN, HIGH); delay(pPulseWidth);
		digitalWrite(LED_BUILTIN, LOW); delay(pPulseWidth);
	}

}// end of ::flashLED
//--------------------------------------------------------------------------------------------------


#endif // _TOOLS_H
