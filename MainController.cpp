//--------------------------------------------------------------------------------------------------
// MainController.cpp
//
//
// Main code for the Spud Main Pi Backpack.
//
// In the Arduino IDE, choose settings (must be done every time you switch projects!!!):
//
//	in "Tools" on Main Menu
//
//		Board: Board/Ardunino AVR Boards/Arduino Pro or Pro Mini
//		Processor: ATmega328P(3.3V, 8MHz)
//		Programmer: ArduinoISP
//
// The ArduinoISP programmer is used for both an FTDI adapter or with direct TTL connection such as
// on Spud.
//
// When using the Arduino IDE Serial Monitor, select baud rate 9600
//
//  Board Files Required:
//
//
//  Library Files Required:
//
//
//	Library file of future interest:
//
//
//
//----------------------------------------------------
//
// Authors:
//    Mike Schoonover 10/24/2020
//
//--------------------------------------------------------------------------------------------------


#include "MainController.h"

#define DEBUG_VERBOSE_CONTROLLER 1	// displays extra debugging messages on serial port

//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
// class MainController
//
// This class is the main MainController (as in MVC structure).
//

//--------------------------------------------------------------------------------------------------
// MainController::MainController (constructor)
//

MainController::MainController()
{

}// end of MainController::MainController (constructor)
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::init
//
/*
 * top-level init that handles basic initialization and calls initSelf for specific init
 * Must be called after instantiation to complete setup.
 *
 */

void MainController::init()
{

	setupIO();

	initSerialPort();

	Serial.setTimeout(SERIAL_TIMEOUT_MILLIS); //native USB serial port to host (and programming)

	settings = new Settings();
	settings->init();

	PacketTool *packetTool;
	packetTool = new PacketTool();
	packetTool->init(HOST_ADDRESS, THIS_DEVICE_ADDRESS_ON_NETWORK);
	packetTool->setStreams(&Serial, &Serial);

	hostCom = new HostSerialComHandler(THIS_DEVICE_ADDRESS_ON_NETWORK, packetTool);
	hostCom->init();

	neckMotor = new NeckMotor();
	neckMotor->init();

//	flashLED(6, 300); //debug mks

	hostCom->getPacketTool()->sendCString(
						PacketTypeEnum::LOG_MESSAGE, "First connection to Spud since restart...");

//	hostCom->echoTest();

//	hostCom->waitForNumberOfBytes(3, 60000);

//	hostCom->sendBytes(7,72,101,108,108,111,10,13); //debug mks "HelloCRLF"

//	Serial.println("test 1"); //debug mks

//	hostCom->sendBytes(HostSerialComHandler::PacketTypeEnum::LOG_MESSAGE, 4, 1, 2, 3, 4); //debug mks

// debug mks ~ for reference
//    sendBytes(HEAD_TRANSLATE_CMD,
//             (byte) ((pDistance >> 8) & 0xff), (byte) (pDistance & 0xff));

}// end of MainController::init
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::process
//
/**
 * This method is called every iteration in the main loop in order to perform processes.
 *
 * Note that the !Serial() check does not always (ever?) detect that the host has closed the
 * connection. This is usually not a problem as the host can reconnect without re-initializing the
 * serial port on this side.
 *
 */

void MainController::process()
{

	if(!Serial){ initSerialPort(); }

	bool packetReady = hostCom->getPacketTool()->checkForPacketReady();

	if(packetReady){ handlePacket(); }

	neckMotor->process();

	//debug mks
	//char msg[75];
	//sprintf(msg, "Test : %d", neckMotor->getStepCount());
	//hostCom->getPacketTool()->sendCString(PacketTypeEnum::LOG_MESSAGE, msg);
	//debug mks end

	doCommands();

}// end of MainController::process
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handlePacket
//
/**
 * Processes the packet which has been received in the HostSerialComHandler object.
 *
 */

void MainController::handlePacket()
{

	PacketTypeEnum pktType = hostCom->getPacketTool()->getPktType();

	PacketStatusEnum status;

	switch(pktType){

		case PacketTypeEnum::GET_DEVICE_INFO :
			hostCom->getPacketTool()->sendCString(PacketTypeEnum::LOG_MESSAGE,
															"Hello from Spud's Head Pi Backpack!");
			return;
		break;

		case PacketTypeEnum::LOG_MESSAGE :
			//in the future may display message on a small screen
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		case PacketTypeEnum::START_RANDOM_NECK_TURNS :
			neckMotor->enableRandomHeadTurningRoutine(true);
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		case PacketTypeEnum::STOP_RANDOM_NECK_TURNS :
			neckMotor->enableRandomHeadTurningRoutine(false);
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		case PacketTypeEnum::START_FULL_NECK_TURNS :
			neckMotor->enableFullHeadTurningRoutine(true);
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		case PacketTypeEnum::STOP_FULL_NECK_TURNS :
			neckMotor->enableFullHeadTurningRoutine(false);
			hostCom->sendACKPkt(pktType, PacketStatusEnum::PACKET_VALID);
			return;
		break;

		case PacketTypeEnum::SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL :
			status = handleSetNeckMotorDriveCurrentLevel();
			hostCom->sendACKPkt(pktType, status);
			return;
		break;

		case PacketTypeEnum::TURN_NECK_SPECIFIED_STEPS :
			status=handleNeckTurnSpecifiedSteps();
			hostCom->sendACKPkt(pktType, status);
			return;
		break;

		default:
		
		break;

	}

}// end of MainController::handlePacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::doCommmands
//
/**
 * If commandCode has been set to anything other than CBC_NO_CMD, the command specified by
 * the value is executed.
 * 
 * This is the preferred method for the GUI controls to invoke an action by the controller.
 * 
 * Alternatively, a callback can be set in a GUI control which it can call to invoke an action.
 * This is less preferable because it requires a callback pointer to be passed to the GUI.
 * 
 */

void MainController::doCommands()
{

/*
	switch(shared.getCommandCode()){

	case CallBackCommand::CBC_NO_CMD :
		return; //no action
	break;

	case CBC_PRINT_CURRENT_CONFIG_FILE_TO_DISPLAY :

		displayFileOnPage("01 - Sidewinder Test Configuration.ini");

	break;

	case CBC_PRINT_CURRENT_CAL_FILE_TO_DISPLAY :

		displayFileOnPage("Job 1 Calibration.ini");

	break;

	case CBC_KEYBOARD_OPEN:

		if(shared.settingToModify) {
			keyboardPage->open(shared.settingToModify->getValue(),
					shared.settingToModify->getMaxChars());
		}

	break;

	case CBC_KEYBOARD_RETURN:

		if(shared.settingToModify && shared.keyboardBufferModified) {
			shared.settingToModify->setValue(shared.keyboardBuffer);
		}

		shared.settingToModify = nullptr;
		shared.keyboardBufferModified = false;

	break;

	}

 *
 */

}// end of MainController::doCommands
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handleSetNeckMotorDriveCurrentLevel
//
/**
 * Handles SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL packets.
 *
 * @return	PacketStatusEnum::PACKET_VALID if no error; a PacketStatusEnum error code on error
 *
 */

PacketStatusEnum MainController::handleSetNeckMotorDriveCurrentLevel()
{

	MotorCurrentLevel driveCurrentLevel;
	int16_t driveCurrentLevelAsInt;

	int16_t driveCurrentPWMLevel;

	int pktIndex = 0;

	PacketStatusEnum status = hostCom->getPacketTool()->
								parseDuplexIntegerFromPacket(&pktIndex, &driveCurrentLevelAsInt);

	driveCurrentLevel = (MotorCurrentLevel)driveCurrentLevelAsInt;

	if(status != PacketStatusEnum::PACKET_VALID){ return(status); }

	status = hostCom->getPacketTool()->
								parseDuplexIntegerFromPacket(&pktIndex, &driveCurrentPWMLevel);

	if(status != PacketStatusEnum::PACKET_VALID){
		hostCom->getPacketTool()->sendCString(PacketTypeEnum::LOG_MESSAGE, "Packet error!"); //debug mks
		return(status);
	}

	neckMotor->setMotorDriveCurrentLevel(driveCurrentLevel, driveCurrentPWMLevel);

	//debug mks

	char msg[75];

	sprintf(msg, "Neck Motor Drive Current Level set to : %d, %d",
			(int)neckMotor->getMotorDriveCurrentLevel(), neckMotor->getMotorDriveCurrentPWMLevel());

	hostCom->getPacketTool()->sendCString(PacketTypeEnum::LOG_MESSAGE, msg);

	//debug mks end

	return(PacketStatusEnum::PACKET_VALID);

}// end of MainController::handleSetNeckMotorDriveCurrentLevel
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handleNeckTurnSpecifiedSteps
//
/**
 * Handles TURN_NECK_SPECIFIED_STEPS packets. The integer number of steps to turn and the
 * integer delay value for each step (speed control) are extracted from the packet.
 *
 * Invokes the turn routine in the neckMotor object using the extracted values.
 *
 * @return	PacketStatusEnum::PACKET_VALID if no error; a PacketStatusEnum error code on error
 *
 */

PacketStatusEnum MainController::handleNeckTurnSpecifiedSteps()
{

	int16_t numStepsToTurn = 0;
	int16_t pNeckStepDelayMS;

	int pktIndex = 0;

	PacketStatusEnum status = hostCom->getPacketTool()->
										parseDuplexIntegerFromPacket(&pktIndex, &numStepsToTurn);

	if(status != PacketStatusEnum::PACKET_VALID){ return(status); }

	status = hostCom->getPacketTool()->parseDuplexIntegerFromPacket(&pktIndex, &pNeckStepDelayMS);

	if(status != PacketStatusEnum::PACKET_VALID){ return(status); }

	neckMotor->startNeckTurnSpecifiedSteps(numStepsToTurn, pNeckStepDelayMS);

	return(PacketStatusEnum::PACKET_VALID);

}// end of MainController::handleNeckTurnSpecifiedSteps
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::setupIO
//
// Sets up generic Digital and Analog Input/Output pins. Pins for dedicated functions such
// as SPI, U2C, or PWM are set up in other functions.
//

void MainController::setupIO()
{

	// initialize digital pin LED_BUILTIN as an output.
	pinMode(LED_BUILTIN, OUTPUT);

	digitalWrite(LED_BUILTIN, LOW);

/*
  // initialize all buttons with pullup
  
  pinMode(LEFT_BTN_1, INPUT_PULLUP);
  pinMode(LEFT_BTN_2, INPUT_PULLUP);
  pinMode(LEFT_BTN_3, INPUT_PULLUP);
  pinMode(LEFT_BTN_4, INPUT_PULLUP);
  pinMode(LEFT_BTN_5, INPUT_PULLUP);

  pinMode(RIGHT_BTN_1, INPUT_PULLUP);
  pinMode(RIGHT_BTN_2, INPUT_PULLUP);
  pinMode(RIGHT_BTN_3, INPUT_PULLUP);
  pinMode(RIGHT_BTN_4, INPUT_PULLUP);
  pinMode(RIGHT_BTN_5, INPUT_PULLUP);

  pinMode(BOTTOM_BTN_1, INPUT_PULLUP);
  pinMode(BOTTOM_BTN_2, INPUT_PULLUP);
  pinMode(BOTTOM_BTN_3, INPUT_PULLUP);
  pinMode(BOTTOM_BTN_4, INPUT_PULLUP);
  pinMode(BOTTOM_BTN_5, INPUT_PULLUP);
  pinMode(BOTTOM_BTN_6, INPUT_PULLUP);

	pinMode(SPARE_IO_1, INPUT_PULLUP);
	pinMode(SPARE_IO_2, INPUT_PULLUP);
	pinMode(SPARE_IO_3, INPUT_PULLUP);

	pinMode(FRONT_PANEL_POWER_SWITCH, INPUT_PULLUP);

  pinMode(FRONT_PANEL_LED, OUTPUT);

  pinMode(RA8875_WAIT_INPUT, INPUT);
  pinMode(RA8875_INT_INPUT, INPUT_PULLDOWN);

 *
 */

}// end MainController::setupIO
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::initSerialPort
//
/**
 * Initializes the port named Serial. This is typically the USB-to-serial port.
 *
 * When using the boards without M4 chips, this function waited until the Serial object
 * became active. When the Ardunio Serial Monitor Panel was not open, this caused a
 * lockup. This also caused a lockup in standalone mode (not connected to Arduino IDE).
 *
 * On some non-M4 boards, a 1.5 second delay after Serial.begin is required or the
 * first serial transmissions will be lost.
 *
 * The original Ardunio code comment stated that the wait is only necessary for a
 * "native USB port".
 *
 */

void MainController::initSerialPort()
{

	Serial.begin(9600);

	flashLED(2, 300);

	while(!Serial);

//	flashLED(3, 100);

	Serial.write('A');

}// end of MainController::initSerialPort
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::displayLoopExecutionTime
//
// This function calculates the time it has taken to run pNumLoopsRun number of execution
// loops.
//
// Global startTime should have been started before the first loop was run.
//
// Time Analysis at Various SPI speeds:
//
//  Tools->Max SPI: "24 MHz (standard)"
//  
//  4  Mhz SPI -> .22 seconds per loop (text works)
//  6  Mhz SPI -> .20 seconds per loop (text works)
//  8  Mhz SPI -> .18 seconds per loop (text fails)
//  12 Mhz SPI -> .17 seconds per loop (text fails)
//  24 Mhz SPI -> .15 seconds per loop (text fails)
//
// NOTHING for SPI speeds up the BMP image display -- QSPI speed issue?
// Tried setting to CPU/2 in Tools, but no improvement.
//

void MainController::displayLoopExecutionTime(const int pNumLoopsRun) {

    int timePassed = (millis() - startTime);

    double timePerLoop = timePassed / pNumLoopsRun;

//debug mks -- can't use printf if not ARM processor    Serial.printf("Time required for %d loops: %d ms", pNumLoopsRun, timePassed);
    //printDoubleToSerialPort(timePassedSecs , 100);
    //Serial.printf("Milliseconds per loop: ");
    //printDoubleToSerialPort(timePerLoop , 1000);
    //Serial.println("");

}// end MainController::displayLoopExecutionTime
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// debug
//

void MainController::debug() {

	return; // debug nps debug the debug? wtf

	//printBarToSerial('-', 80);
	//Serial.println("-- Debug Section\n");

}// end debug
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::handleErrorFlags
//
// Handles errors based on error codes, printing the proper error messages to serial output.
// todo: the error messages should later be written to an error log file.
//

void MainController::handleErrorFlags(const ErrorFlag pErrorFlag){

/*

	if(pErrorFlag == NO_ERROR) return;

	Warning warning(0, "Warning", "", "Warning0", &shared, 150, 200, 500, 80, RA8875_WHITE,
															LIGHT_GRAY_16BPP, &display, nullptr);
	std::string errorMsg = "ERROR ";

	Serial.print("Error Code: "); Serial.print(pErrorFlag);

	switch(pErrorFlag){
	case ERROR_MALLOC:
		Serial.println("MALLOC ERROR");
		exit(ERROR_MALLOC);   // todo: handle malloc error without exiting
		break;
	case ERROR_ADDINGCHILD:
		Serial.println("ERROR ADDING CHILD TO CHILD ARRAY");
		exit(ERROR_ADDINGCHILD);
		break;
	case ERROR_ADDINGPAGE:
		Serial.println("ERROR ADDING PAGE TO PAGE ARRAY");
		exit(ERROR_ADDINGPAGE);
		break;
	case ERROR_INIT_SPIFLASH:
		Serial.println(" INITIALIZING SPI FLASH MEMORY");
		exit(ERROR_INIT_SPIFLASH);
		break;
	case ERROR_CORRUPT_INI_FILE:
		Serial.println((errorMsg += "CORRUPT INI FILE").c_str());
		warning.show(errorMsg.c_str(), -1);
		break;
	default:
		Serial.println("UNKNOWN EXCEPTION CODE");
		break;
	}

 *
 */

}// end of MainController::handleErrorFlags
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// MainController::MainController (destructor)
//

MainController::~MainController()
{

	delete(hostCom);

	delete(neckMotor);

}// end of MainController::MainController (destructor)
//--------------------------------------------------------------------------------------------------

//end of class MainController
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
